package com.zuitt.wdc044.models;

import java.io.Serializable;

public class JwtResponse {
    // Properties
    private static final long serialVersionUID = 756681551955508792L;
    private final String jwtToken;

    // Constructor
    public JwtResponse(String jwtToken){
        this.jwtToken = jwtToken;
    }

    // Getter
    public String getJwtToken(){
        return this.jwtToken;
    }
}
