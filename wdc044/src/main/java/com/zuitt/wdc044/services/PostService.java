package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface PostService {
    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();
    ResponseEntity<?> updatePost(Long id, String stringToken, Post post);

    ResponseEntity<?> deletePost(Long id, String stringToken);


    // Create a feature for retrieving a list of all a user's posts. This feature needs a controller method run by a GET request to the /myPosts endpoint. It should call another method in the service implementation, and passes a string token from the user's JWT.
    Iterable<Post> getMyPosts(String stringToken);
}
